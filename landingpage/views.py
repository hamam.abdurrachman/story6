from django.shortcuts import render, redirect
from .models import Status
from .forms import Status_Form
from django.utils import timezone

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = Status_Form(request.POST)

        if form.is_valid():
            status = Status(status=form.cleaned_data['status'])
            status.save()
            return redirect('/')
    context={
        'data': Status.objects.all().order_by('-time'),
        'form': Status_Form()
    }

    return render(request, 'index.html', context)
