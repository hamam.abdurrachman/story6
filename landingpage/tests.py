from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import Status_Form
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_invalid(self):
        response = Client().get('/gaada/')
        self.assertEqual(response.status_code, 404)

    def test_story6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_tulisan_in_landing_page(self):
        response  = Client().get('/')
        self.assertContains(response, 'Halo, apa kabar?')

    def test_model_can_create_new_status(self):
        status = Status.objects.create(status='ini status')
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_can_save_a_post_request(self):
        response = Client().post('/', data={'status': 'cek status'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = Client().get('/')
        self.assertContains(new_response, 'cek status')

    def test_status_more_than_300_characters(self):
        status = 'ini kalimat yang panjaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaang'
        form = Status_Form(data={'status': status})
        self.assertFalse(form.is_valid())

class Story6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        status.send_keys('Coba Coba')
        time.sleep(2)
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn('Coba Coba', selenium.page_source)