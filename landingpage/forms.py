from django import forms
from .models import Status

class Status_Form(forms.Form):
    status = forms.CharField(label='status', max_length=300, 
                            widget=forms.TextInput(attrs={'class': 'form-control'}))
